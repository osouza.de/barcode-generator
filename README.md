# osouza.de - Barcode Generator

We can use this to generate EAN-13 barcodes and also EAN-8 barcodes (providing only 4 digits and the code will complete the missing 5).

Developed to be used at my girlfriend's job.