import React, { useState } from 'react';
import JsBarcode from 'jsbarcode';
import copyImageToClipboard from './Utils';

function EAN8Generator() {
  const [productCode, setProductCode] = useState('');
  const [barcodeImage, setBarcodeImage] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');
  const handleInputChange = (event) => {
    const inputValue = event.target.value;
    // Allow only numbers and limit to 4 digits
    const filteredValue = inputValue.replace(/\D/g, '').slice(0, 4);
    setProductCode(filteredValue);
  };

  const generateBarcode = () => {
    if (productCode.length !== 4) {
      setErrorMessage('Por favor, insira 4 números para o código.');
      setBarcodeImage(null);
      return;
    }
    setErrorMessage(null);

    // Calculate a valid EAN-8 code 
    const paddedInputCode = productCode.padStart(4, '0'); 
    const calculatedEan8 = calculateEAN8(paddedInputCode); 
    const canvas = document.createElement('canvas');
    JsBarcode(canvas, calculatedEan8, {
      format: "EAN8",
      displayValue: true,
      height: 70,
    });
    const barcodeImageUrl = canvas.toDataURL('image/png');
    setBarcodeImage(barcodeImageUrl);

    // Copy barcode image to clipboard
    copyImageToClipboard(barcodeImageUrl);
  };

  // Function to calculate a valid EAN-8 code 
  function calculateEAN8(inputCode) {
    let sumOdd = 0;
    let sumEven = 0;

    // Calculate sum of digits at odd and even positions (from right to left)
    for (let i = inputCode.length - 1; i >= 0; i--) {
      const digit = parseInt(inputCode.charAt(i), 10);
      if ((inputCode.length - i) % 2 === 1) { // Odd positions (from right)
        sumOdd += digit;
      } else { // Even positions (from right)
        sumEven += digit;
      }
    }
    // Calculate check digit using the provided formula
    const checkDigit = (10 - ((3 * sumOdd + sumEven) % 10)) % 10; 
    // Construct the final EAN-8 code 
    const calculatedEan8 = "000" + inputCode + checkDigit; 
    return calculatedEan8;
  }

  return (
    <div className="barcode-generator-container">
      <h2>Código EAN-8</h2>
      <div className="input-group">
        <input
          type="text"
          value={productCode}
          onChange={handleInputChange}
          placeholder="Digite os 4 dígitos do produto" 
        />
        <span className="character-count">{productCode.length}/4</span> 
      </div>
      <button 
        className="generate-button" 
        onClick={generateBarcode} 
      >
        Gerar e copiar imagem
      </button>
      {errorMessage && <p className="error-message">{errorMessage}</p>}
      {barcodeImage && (
        <div className="barcode-image">
          <img src={barcodeImage} alt="Generated Barcode" />
        </div>
      )}
    </div> 
  );
}
export default EAN8Generator;