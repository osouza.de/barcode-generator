import { ToastContainer } from 'react-toastify';
import EAN13Generator from './EAN13Generator';
import EAN8Generator from './EAN8Generator';
import './App.css'

function App() {

  return (
    <>
      <div>
        <EAN13Generator />
        <hr></hr>
        <EAN8Generator />
      </div>
      <ToastContainer position="bottom-center" />
    </>
  )
}

export default App
