import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'; // Import CSS for styling

// Function to copy image to clipboard
const copyImageToClipboard = (imageUrl) => {
    const img = new Image();
    img.onload = () => {
        const canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        canvas.toBlob((blob) => {
        navigator.clipboard.write([new ClipboardItem({ 'image/png': blob })])
            .then(() => {
            console.log('Barcode image copied to clipboard!');
            toast.success('Código de barras gerado e copiado. Agora é só COLAR!'); // Show success toast
            // You could optionally display a success message to the user here
            })
            .catch(err => {
            console.error('Failed to copy barcode image: ', err);
            toast.error('Erro ao copiar imagem do código de barras. - chama o Dennis!'); // Show error toast
            // You could display an error message to the user if copying fails
            });
        });
    };
    img.src = imageUrl;
};


export default copyImageToClipboard;