import React, { useState } from 'react';
import JsBarcode from 'jsbarcode';
import copyImageToClipboard from './Utils';

function EAN13Generator() {
  const [barcodeNumber, setBarcodeNumber] = useState('');
  const [barcodeImage, setBarcodeImage] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');

  const handleInputChange = (event) => {
    let inputValue = event.target.value;
    
    // generateBarcode();
    
    // Filter out non-numeric characters
    inputValue = inputValue.replace(/\D/g, ''); 

    // Limit to 13 characters
    if (inputValue.length <= 13) {
      setBarcodeNumber(inputValue);
    } else {
      // generateBarcode();
    }

    // Clear potential previous error messages
    if (errorMessage) {
      setErrorMessage('');
    }
  };

  const isEan13Valid = (barcode) => {
    if (barcode.length !== 13) {
      return false;
    }
    let sumOdd = 0;
    let sumEven = 0;
    for (let i = 0; i < 12; i++) {
      if (i % 2 === 0) {
        sumEven += parseInt(barcode.charAt(i));
      } else {
        sumOdd += parseInt(barcode.charAt(i));
      }
    }
    const calculatedCheckDigit = (10 - ((sumOdd * 3) + sumEven) % 10) % 10;
    return parseInt(barcode.charAt(12)) === calculatedCheckDigit;
  };

  const generateBc = () => {
    // Clear the previous barcode image first
    setBarcodeImage(null); 
    if (isEan13Valid(barcodeNumber)) {
      const canvas = document.createElement('canvas');
      JsBarcode(canvas, barcodeNumber, { format: 'EAN13' });
      const barcodeImageUrl = canvas.toDataURL('image/png');
      setBarcodeImage(barcodeImageUrl);
      
      // Copy barcode image to clipboard
      copyImageToClipboard(barcodeImageUrl);
    } else {
      setErrorMessage('Por favor, insira um código de barras EAN-13 válido.');
    }
  };

  // const isBarcodeValid = barcodeNumber.length === 13;

  return (
    <div className="barcode-generator-container">
      <h2>Código EAN-13</h2>
      <div className="input-group">
        <input
          type="text"
          value={barcodeNumber}
          onChange={handleInputChange}
          placeholder="Digite um código EAN-13" 
        />
        <span className="character-count">{barcodeNumber.length}/13</span> 
      </div>
      <button 
        className="generate-button" 
        onClick={generateBc} 
      >
        Gerar e copiar imagem
      </button>
      {errorMessage && <p className="error-message">{errorMessage}</p>}
      {barcodeImage && (
        <div className="barcode-image">
          <img src={barcodeImage} alt="Generated Barcode" />
        </div>
      )}
    </div> 
  );
}

export default EAN13Generator;